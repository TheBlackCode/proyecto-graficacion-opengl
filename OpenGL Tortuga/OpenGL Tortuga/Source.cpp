﻿#include <glut.h>
#include <Windows.h>

void display() {
	glClearColor(1.0, 1.0, 1.0,0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0, 0.0, 0.0);
	glutWireTorus(0.25, 0.75, 26, 28);
	glColor3f(0.0, 0.0, 1.0);
	glutWireCube(.60);
	glutSwapBuffers();
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(1280, 720);
	glutInitWindowPosition(20, 20);
	glutCreateWindow("Tortuga");
	glutDisplayFunc(display);
	glutMainLoop();
	return 0;
}

