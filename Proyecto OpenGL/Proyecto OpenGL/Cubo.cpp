﻿

#include <Windows.h>
#include <glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#define M_PI 3.14159265358979323846
using namespace std;

double rotate_y = 0;
double rotate_x = 0;
double rotate_z = 0;

double rotate_y2 = 0;
double rotate_x2 = 0;
double rotate_z2 = 0;

double color_r = 0;
double color_b = 0;
double color_g = 0;


double filas = 1;
double columnas = 1;

GLfloat X = 0.0f;
GLfloat Y = 0.0f;
GLfloat Z = 0.0f;
float scale = 1.0f;

bool bx = true;
bool by = true;
bool bz = true;
bool ax = true;
bool ay = true;
bool az = true;
bool cr = false;
bool cg = false;
bool cb = false;
void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void ArrowKey(int key, int x, int y) {
	//Código
	switch (key) {
	case GLUT_KEY_RIGHT:
		columnas += 1.0;
		break;
		//  Flecha izquierda: disminuir rotación 5 grados
	case GLUT_KEY_LEFT:
		columnas -= 1.0;
		break;
	case GLUT_KEY_UP:
		filas+= 1.0;
		break;
	case GLUT_KEY_DOWN:
		filas -= 1.0;
		break;
	}

	glutPostRedisplay();
}

void dibujaCubo() {

	//LADO FRONTAL: lado multicolor
	glBegin(GL_POLYGON);
	glColor3f(color_r, 0.5,0.2);    
	glVertex3f(0.5, -0.5, -0.5);      // P1 es rojo
	glColor3f( 0.3,0.9, color_b );    
	glVertex3f(0.5, 0.5, -0.5);      // P2 es verde
	glColor3f(0.8, color_g, 0.4);
	glVertex3f(-0.5, 0.5, -0.5);      // P3 es azul
	glColor3f(color_r, color_g, 0.3);
	glVertex3f(-0.5, -0.5, -0.5);      // P4 es morado

	glEnd();

	// LADO TRASERO: lado amarillo
	glBegin(GL_POLYGON);
	glColor3f(0.5, 1.0, 0.0);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glEnd();

	// LADO DERECHO: lado morado
	glBegin(GL_POLYGON);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glEnd();

	// LADO IZQUIERDO: lado verde
	glBegin(GL_POLYGON);
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();

	// LADO SUPERIOR: lado azul
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.5, 0.5, 0.5);
	glVertex3f(0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, -0.5);
	glVertex3f(-0.5, 0.5, 0.5);
	glEnd();


	// LADO INFERIOR: lado verde fuerte
	glBegin(GL_POLYGON);
	glColor3f(0.0, 0.4, 0.3);
	glVertex3f(0.5, -0.5, -0.5);
	glVertex3f(0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, 0.5);
	glVertex3f(-0.5, -0.5, -0.5);
	glEnd();


}

void dibujaPiramide() {
	glBegin(GL_TRIANGLES);
	glColor3f(color_r, 0.3, color_b);
	glVertex3d(-3, 0, 3);
	glVertex3d(-3, 0, -3);
	glVertex3d(3, 0, 0);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0.2, color_g, color_b);
	glVertex3d(0, 6, 0);
	glVertex3d(-3,0,3);
	glVertex3d(3, 0, 0);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(color_r, color_g, 0.6);
	glVertex3d(0, 6, 0);
	glVertex3d(-3, 0, 3);
	glVertex3d(3, 0, 0);
	glEnd();

	glBegin(GL_TRIANGLES);
	glColor3f(0, 0, color_b);
	glVertex3d(0, 6, 0);
	glVertex3d(-3, 0, 3);
	glVertex3d(-3, 0, -3);
	glEnd();
	
}

void colores() {
	if (color_r >= 1) {
		cr = true;
	}
	else if(color_r<=0) {
		cr = false;
	}
	if (color_g >= 1) {
		cg = true;
	}
	else if(color_g<=0) {
		cg = false;
	}
	if (color_b >= 1) {
		cb = true;
	}
	else if(color_b<=0){
		cb = false;
	}
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	gluLookAt(0.0 - X, 0.0 - Y, 4.5 - Z, 0.0 - X, 0.0 - Y, 0.0 - Z, 0.0, 1.0, 0.0);//(0.0,0.0,0.5,0.0,0.0,0.0,0.0,1.0,0.0);
	glPushMatrix();
	if (bx == true) {
		rotate_x += .2;
	}
	if (by == true) {
		rotate_y += .2;
	}
	if (bz == true) {
		rotate_z += .2;
	}
	colores();
	if (cr == true) {
		color_r -= 0.0001;
	}
	else {
		color_r += 0.0001;
	}
	if (cg == true) {
		color_g -= 0.0001;
	}
	else {
		color_g += 0.0001;
	}
	if (cb == true) {
		color_b -= 0.0001;
	}
	else {
		color_b += 0.0001;
	}
	glScalef(0.5, 0.5, 0.5);
	glTranslatef(-0.5, -0.5, 0);
	glRotatef(rotate_x, 1.0, 0.0, 0.0);
	glRotatef(rotate_y, 0.0, 1.0, 0.0);
	glRotatef(rotate_z, 0.0, 0.0, 1.0);
	dibujaCubo();
	
	glPopMatrix();


	//triangulo
	glPushMatrix();
	glTranslatef(0.5, 0.5, 0);
	glScalef(0.1, 0.1, 0.1);
	if (ax == true) {
		rotate_x2 += .2;
	}
	if (ay == true) {
		rotate_y2 += .2;
	}
	if (az == true) {
		rotate_z2 += .2;
	}
	glRotatef(rotate_x2, 1.0, 0.0, 0.0);
	glRotatef(rotate_y2, 0.0, 1.0, 0.0);
	glRotatef(rotate_z2, 0.0, 0.0, 1.0);
	dibujaPiramide();
	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -1.00, 1.0, 1.0, 50.0);
	glMatrixMode(GL_MODELVIEW);

}

void keyboard(unsigned char key, int x, int y) {
	switch (key)
	{

	case 43:
		Z += 1.0;
		break;
	case 45:
		Z -= 1.0;
		break;
	case 120:
		if (by == false) {
			by = true;
		}
		else {
			by = false;
		}
		break;
	case 121:
		if (bx == false) {
			bx = true;
		}
		else {
			bx = false;
		}
		break;
	case 122:
		if (bz == false) {
			bz = true;
		}
		else {
			bz = false;
		}
		break;
	case 97:
		if (ay == false) {
			ay = true;
		}
		else {
			ay = false;
		}
		break;
	case 115:
		if (ax == false) {
			ax = true;
		}
		else {
			ax = false;
		}
		break;
	case 100:
		if (az == false) {
			az = true;
		}
		else {
			az = false;
		}
		break;
	case 27:
		exit(0);
		break;
	}
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(400, 400);

	glutInitWindowPosition(100, 100);
	glutCreateWindow("Cubo 3D");
	glEnable(GL_DEPTH_TEST);
	init();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(ArrowKey);
	glutMainLoop();
	return 0;
}